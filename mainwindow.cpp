#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	connect(ui->pushButton,
		SIGNAL(clicked()),
		this,
		SLOT(clickedButton()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clickedButton()
{
	ui->label->setText("The button was clicked");
}
